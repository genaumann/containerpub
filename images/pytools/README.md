# Python Tools

- Alpine 3.18 based

## Variables

- `PYVERSION` to set the python version of the base image

## Python Content

- anybadge
- pylint
- pylint-exit

## Examples GitLab CI

### pylint with pylint-exit and anybadge

```yml
pylint:
  stage: lint
  image: quay.io/genaumann/pub/pytools:3.12
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
  artifacts:
    when: always
    expire_in: 2h
    paths:
      - pylint.svg
  script:
    - RESULT=$(pylint cib) || EXIT_CODE=$?
    - echo "$RESULT"
    - pylint-exit $EXIT_CODE
    - echo "$RESULT" > pylint.txt
    - SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' pylint.txt)
    - anybadge --value=$SCORE --file=pylint.svg pylint
```
