# Public Images

This repo contains all public images off genaumann.

| Registry | URL                    |
| -------- | ---------------------- |
| Quay.io  | `quay.io/genauman/pub` |
